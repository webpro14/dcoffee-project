import { Bill } from 'src/bill/entities/bill.entity';
import { Material } from 'src/materials/entities/material.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class BillDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  bill_detail_name: string;

  @Column()
  bill_detail_amount: number;

  @Column()
  bill_detail_price: number;

  @Column()
  bill_detail_total: number;

  @ManyToOne(() => Bill, (bill) => bill.bill_detail)
  bill: Bill;

  @ManyToOne(() => Material, (material) => material.bill_detail)
  material: Material;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updateDate: Date;

  @DeleteDateColumn()
  deleteDate: Date;
  bill_detail: any;
}
