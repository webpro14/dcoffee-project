import { CheckMaterialDetail } from 'src/check_material_detail/entities/check_material_detail.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class CheckMaterial {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  check_mat_date: Date;

  @Column()
  check_mat_time: string;

  @ManyToOne(() => Employee, (employee) => employee.check_material)
  employee: Employee;

  @OneToMany(
    () => CheckMaterialDetail,
    (check_material_detail) => check_material_detail.check_mat,
  )
  check_material_detail: CheckMaterialDetail;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updateDate: Date;

  @DeleteDateColumn()
  deleteDate: Date;
}
