import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Repository } from 'typeorm';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';
import { Order } from 'src/orders/entities/order.entity';
import { CheckTime } from 'src/check-time/entities/check-time.entity';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
    @InjectRepository(User)
    private userRepository: Repository<User>, // @InjectRepository(Order) // private ordersRepository: Repository<Order>, // @InjectRepository(CheckTime) // private checktimeRepository: Repository<CheckTime>,
  ) {}

  async create(createEmployeeDto: CreateEmployeeDto) {
    const user = await this.userRepository.findOneBy({
      id: createEmployeeDto.userId,
    });
    const employee: Employee = new Employee();
    employee.user = user;
    employee.name = createEmployeeDto.name;
    employee.house_no = createEmployeeDto.house_no;
    employee.village_no = createEmployeeDto.village_no;
    employee.sub_district = createEmployeeDto.sub_district;
    employee.district = createEmployeeDto.district;
    employee.province = createEmployeeDto.province;
    employee.tel = createEmployeeDto.tel;
    employee.email = createEmployeeDto.email;
    employee.position = createEmployeeDto.position;
    employee.hourly_wage = createEmployeeDto.hourly_wage;
    employee.image = createEmployeeDto.image;
    employee.user.id = createEmployeeDto.userId;
    await this.employeesRepository.save(employee);
    return await this.employeesRepository.findOne({
      where: { id: employee.id },
      relations: ['user'],
    });
  }

  findAll() {
    return this.employeesRepository.find({ relations: ['user', 'checkTime'] });
  }

  async findOne(id: number) {
    const employee = await this.employeesRepository.findOne({
      where: { id: id },
      relations: ['user', 'checkTime'],
    });
    if (!employee) {
      throw new NotFoundException();
    }
    return employee;
  }

  // async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
  //   try {
  //     const updatedEmployee = await this.employeesRepository.save({
  //       id,
  //       ...updateEmployeeDto,
  //     });
  //     return updatedEmployee;
  //   } catch (e) {
  //     throw new NotFoundException();
  //   }
  // }

  // async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
  //   const employee = await this.employeesRepository.findOne({
  //     where: { id: id },
  //   });
  //   if (!employee) {
  //     throw new NotFoundException();
  //   }
  //   const updatedEmployee = { ...employee, ...updateEmployeeDto };
  //   return this.employeesRepository.save(updatedEmployee);

  // }

  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    const employee = await this.employeesRepository.findOneBy({ id });
    if (!employee) {
      throw new NotFoundException();
    }
    const updatedEmployee = { ...employee, ...updateEmployeeDto };
    return this.employeesRepository.save(updatedEmployee);
  }

  async remove(id: number) {
    const employee = await this.employeesRepository.findOne({
      where: { id: id },
    });
    if (!employee) {
      throw new NotFoundException();
    }
    return this.employeesRepository.remove(employee);
  }
}
