import { IsNotEmpty } from 'class-validator';

export class CreateCheckTimeDto {
  @IsNotEmpty()
  cio_date: string;

  @IsNotEmpty()
  cio_time_in: string;

  @IsNotEmpty()
  cio_time_out: string;

  @IsNotEmpty()
  cio_total_hour: number;

  @IsNotEmpty()
  employeeId: number;

  @IsNotEmpty()
  salaryId: number;
}
