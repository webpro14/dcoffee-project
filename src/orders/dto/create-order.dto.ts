import { Store } from 'src/stores/entities/store.entity';

export class CreateOrderDto {
  queue: number;
  date: Date;
  time: string;
  discount: number;
  amount: number;
  total: number;
  received: number;
  change: number;
  payment: string;
  stores: number;
  userId: number;
  orderItemId: CreateOrderItemDto[];
}
class CreateOrderItemDto {
  name: string;
  // discount: number;
  price: number;
  amount: number;
  total: number;
  order: number;
  product: number;
}
