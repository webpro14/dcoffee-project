import { Test, TestingModule } from '@nestjs/testing';
import { Reports3Service } from './reports3.service';

describe('Reports3Service', () => {
  let service: Reports3Service;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Reports3Service],
    }).compile();

    service = module.get<Reports3Service>(Reports3Service);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
