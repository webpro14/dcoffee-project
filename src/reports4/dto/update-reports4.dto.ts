import { PartialType } from '@nestjs/mapped-types';
import { CreateReports4Dto } from './create-reports4.dto';

export class UpdateReports4Dto extends PartialType(CreateReports4Dto) {}
