import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Employee } from 'src/employees/entities/employee.entity';
import { Salary } from 'src/salarys/entities/salary.entity';
import { Repository } from 'typeorm';
import { CreateCheckTimeDto } from './dto/create-check-time.dto';
import { UpdateCheckTimeDto } from './dto/update-check-time.dto';
import { CheckTime } from './entities/check-time.entity';

@Injectable()
export class CheckTimeService {
  constructor(
    @InjectRepository(CheckTime)
    private checktimeRepository: Repository<CheckTime>,

    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,

    @InjectRepository(Salary)
    private salaryRepository: Repository<Salary>,
  ) {}
  async create(createCheckTimeDto: CreateCheckTimeDto) {
    const employee = await this.employeeRepository.findOneBy({
      id: createCheckTimeDto.employeeId,
    });
    const salary = await this.salaryRepository.findOneBy({
      id: createCheckTimeDto.salaryId,
    });
    const checkTime: CheckTime = new CheckTime();
    checkTime.salary = salary;
    checkTime.employee = employee;
    checkTime.cio_date = createCheckTimeDto.cio_date;
    checkTime.cio_time_in = createCheckTimeDto.cio_time_in;
    checkTime.cio_time_out = createCheckTimeDto.cio_time_out;
    checkTime.cio_total_hour = createCheckTimeDto.cio_total_hour;
    checkTime.employee.id = createCheckTimeDto.employeeId;
    checkTime.salary.id = createCheckTimeDto.salaryId;
    await this.checktimeRepository.save(checkTime);
    return this.checktimeRepository.findOne({
      where: { cio_id: checkTime.cio_id },
      relations: ['salary'],
    });
  }

  findAll() {
    return this.checktimeRepository.find({
      relations: ['employee', 'salary'],
    });
  }
  async findOne(id: number) {
    const checkTime = await this.checktimeRepository.findOne({
      where: { cio_id: id },
      relations: ['employee', 'salary'],
    });
    if (!checkTime) {
      throw new NotFoundException();
    }
    return checkTime;
  }

  async update(id: number, updateCheckTimeDto: UpdateCheckTimeDto) {
    const checkTime = await this.checktimeRepository.findOne({
      where: { cio_id: id },
    });
    if (!checkTime) {
      throw new NotFoundException();
    }
    const updateCheckTime = { ...checkTime, ...updateCheckTimeDto };
    return this.checktimeRepository.save(updateCheckTime);
  }

  async remove(id: number) {
    const checkTime = await this.checktimeRepository.findOne({
      where: { cio_id: id },
    });
    if (!checkTime) {
      throw new NotFoundException();
    }
    return this.checktimeRepository.remove(checkTime);
  }
}
