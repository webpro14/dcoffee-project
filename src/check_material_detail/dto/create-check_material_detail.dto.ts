import { IsNotEmpty } from 'class-validator';
export class CreateCheckMaterialDetailDto {
  @IsNotEmpty()
  cmd_name: string;

  @IsNotEmpty()
  cmd_qty_last: number;

  @IsNotEmpty()
  cmd_qty_remain: number;

  @IsNotEmpty()
  cmd_qty_expire: number;

  @IsNotEmpty()
  matId: number;

  @IsNotEmpty()
  checkMaterialId: number;
}
