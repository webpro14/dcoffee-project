import { Module } from '@nestjs/common';
import { Reports2Service } from './reports2.service';
import { Reports2Controller } from './reports2.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reports2 } from './entities/reports2.entity';

import { Category } from 'src/categorys/entities/category.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Reports2, Category])],
  controllers: [Reports2Controller],
  providers: [Reports2Service],
  exports: [Reports2Service],
})
export class Reports2Module {}
