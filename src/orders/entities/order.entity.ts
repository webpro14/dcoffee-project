import { Customer } from 'src/customer/entities/customer.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  OneToMany,
  Timestamp,
} from 'typeorm';
import { OrderItem } from 'src/orders/entities/order-item.entity';
import { Store } from 'src/stores/entities/store.entity';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class Order {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 0 })
  queue: number;

  @Column()
  date: Date;

  @Column({ default: 0 })
  time: string;

  @Column({ default: 0 })
  discount: number;

  @Column({ default: 0 })
  amount: number;

  @Column({ default: 0 })
  total: number;

  @Column({ default: 0 })
  received: number;

  @Column({ default: 0 })
  change: number;

  @Column({ default: 0 })
  payment: string;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;

  @ManyToOne(() => Store, (stores) => stores.order)
  stores: Store;

  @ManyToOne(() => User, (userId) => userId.order)
  userId: User;

  // @ManyToOne(() => Employee, (employees) => employees.orders)
  // employees: Employee;

  // @ManyToOne(() => Customer, (customer) => customer.orders)
  // customer: Customer; // Customer Id

  @OneToMany(() => OrderItem, (orderItem) => orderItem.order)
  orderItemId: OrderItem;

  // @ManyToOne(() => Employee, (employee) => employee.order)
  // employee: Employee;
}
