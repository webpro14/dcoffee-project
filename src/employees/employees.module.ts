import { Module } from '@nestjs/common';
import { EmployeesService } from './employees.service';
import { EmployeesController } from './employees.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/users/entities/user.entity';
import { Employee } from './entities/employee.entity';
import { Order } from 'src/orders/entities/order.entity';
import { CheckTime } from 'src/check-time/entities/check-time.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Employee, User, Order, CheckTime])],
  controllers: [EmployeesController],
  providers: [EmployeesService],
  exports: [EmployeesService],
})
export class EmployeesModule {}
