import { CheckTime } from 'src/check-time/entities/check-time.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  Column,
  OneToMany,
} from 'typeorm';

@Entity()
export class Salary {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  datess: number;

  @Column()
  workhour: number;

  @Column()
  salary: number;

  @CreateDateColumn()
  CreateDate: Date;

  @UpdateDateColumn()
  UpdateDate: Date;

  @DeleteDateColumn()
  DeleteDate: Date;

  @OneToMany(() => CheckTime, (checkTime) => checkTime.salary)
  checkTime: CheckTime;
}
