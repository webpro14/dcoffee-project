// import { Injectable, NotFoundException } from '@nestjs/common';
// import { CreateOrderItemDto } from './dto/create-order-item.dto';
// import { UpdateOrderItemDto } from './dto/update-order-item.dto';
// import { InjectRepository } from '@nestjs/typeorm';
// import { Order } from 'src/orders/entities/order.entity';
// import { Product } from 'src/products/entities/product.entity';
// import { Repository } from 'typeorm';
// import { OrderItem } from './entities/order-item.entity';

// @Injectable()
// export class OrderItemService {
//   constructor(
//     @InjectRepository(OrderItem)
//     private orderItemsRepository: Repository<OrderItem>,
//     @InjectRepository(Order)
//     private ordersRepository: Repository<Order>,
//     @InjectRepository(Product)
//     private productsRepository: Repository<Product>,
//   ) {}
//   async create(createOrderItemDto: CreateOrderItemDto) {
//     console.log(createOrderItemDto);
//     const order = await this.ordersRepository.findOneBy({
//       id: createOrderItemDto.orderId,
//     });

//     const product = await this.productsRepository.findOneBy({
//       id: createOrderItemDto.productId,
//     });
//     const orderItems: OrderItem = new OrderItem();
//     orderItems.order = order;
//     orderItems.product = product;
//     orderItems.name = createOrderItemDto.name;
//     orderItems.price = orderItems.product.price;
//     orderItems.amount = order.amount + orderItems.amount;
//     orderItems.total = orderItems.price * orderItems.amount;

//     await this.orderItemsRepository.save(orderItems);
//     return await this.orderItemsRepository.findOne({
//       where: { id: orderItems.id },
//       relations: ['order', 'product'],
//     });
//   }

//   findAll() {
//     return this.orderItemsRepository.find({ relations: ['order', 'product'] });
//   }

//   async findOne(id: number) {
//     const orderItems = await this.orderItemsRepository.findOne({
//       where: { id: id },
//       relations: ['order', 'product'],
//     });
//     if (!orderItems) {
//       throw new NotFoundException();
//     }
//     return orderItems;
//   }

//   async update(id: number, updateOrderItemDto: UpdateOrderItemDto) {
//     const orderItems = await this.orderItemsRepository.findOne({
//       where: { id: id },
//     });
//     if (!orderItems) {
//       throw new NotFoundException();
//     }
//     const updatedOrderItem = { ...orderItems, ...updateOrderItemDto };
//     return this.orderItemsRepository.save(updatedOrderItem);
//   }

//   async remove(id: number) {
//     const orderItems = await this.orderItemsRepository.findOne({
//       where: { id: id },
//     });
//     if (!orderItems) {
//       throw new NotFoundException();
//     }
//     return this.orderItemsRepository.softRemove(orderItems);
//   }
// }
