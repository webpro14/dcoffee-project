import { Test, TestingModule } from '@nestjs/testing';
import { Reports3Controller } from './reports3.controller';
import { Reports3Service } from './reports3.service';

describe('Reports3Controller', () => {
  let controller: Reports3Controller;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Reports3Controller],
      providers: [Reports3Service],
    }).compile();

    controller = module.get<Reports3Controller>(Reports3Controller);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
