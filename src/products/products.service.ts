import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Category } from 'src/categorys/entities/category.entity';
import { Like, Repository } from 'typeorm';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Category)
    private categorysRepository: Repository<Category>,
  ) {}

  async create(createProductDto: CreateProductDto) {
    const category = await this.categorysRepository.findOneBy({
      id: createProductDto.categoryId,
    });
    const product: Product = new Product();
    product.category = category;
    product.name = createProductDto.name;
    product.type = createProductDto.type;
    product.size = createProductDto.size;
    product.price = createProductDto.price;
    product.image = createProductDto.image;

    await this.productsRepository.save(product);
    return await this.productsRepository.findOne({
      where: { id: product.id },
      relations: ['category'],
    });
  }

  findByCategory(id: number) {
    return this.productsRepository.find({ where: { categoryId: id } });
  }

  async findAll(query): Promise<Paginate> {
    const cat = query.cat || 1;
    const page = query.page || 1;
    const take = query.take || 10;
    const skip = (page - 1) * take;
    const keyword = query.keyword || '';
    const orderBy = query.orderBy || 'name';
    const order = query.order || 'ASC';
    const currentPage = page;

    const [result, total] = await this.productsRepository.findAndCount({
      relations: ['category'],
      where: { name: Like(`%${keyword}%`) },
      order: { [orderBy]: order },
    });
    const lastPage = Math.ceil(total / take);
    return {
      data: result,
      count: total,
      currentPage: currentPage,
      lastPage: lastPage,
    };
  }

  findOne(id: number) {
    return this.productsRepository.findOne({
      where: { id: id },
    });
  }

  // async update(id: number, updateProductDto: UpdateProductDto) {
  //   try {
  //     const updatedProduct = await this.productsRepository.save({
  //       id,
  //       ...updateProductDto,
  //     });
  //     return updatedProduct;
  //   } catch (e) {
  //     throw new NotFoundException();
  //   }
  // }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productsRepository.findOneBy({ id });
    if (!product) {
      throw new NotFoundException();
    }
    const updateProduct = { ...product, ...updateProductDto };
    return this.productsRepository.save(updateProduct);
  }

  async remove(id: number) {
    const product = await this.productsRepository.findOne({
      where: { id: id },
    });
    try {
      const deletedProduct = await this.productsRepository.remove(product);
      return deletedProduct;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
