import { Employee } from 'src/employees/entities/employee.entity';
import { Salary } from 'src/salarys/entities/salary.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class CheckTime {
  @PrimaryGeneratedColumn()
  cio_id: number;

  @Column()
  cio_date: string;

  @Column()
  cio_time_in: string;

  @Column()
  cio_time_out: string;

  @Column()
  cio_total_hour: number;

  @Column({ default: 1 })
  employeeId: number;

  @ManyToOne(() => Employee, (employee) => employee.checkTime)
  employee: Employee;

  @ManyToOne(() => Salary, (salary) => salary.checkTime)
  salary: Salary;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updateDate: Date;

  @DeleteDateColumn()
  deleteDate: Date;
}
