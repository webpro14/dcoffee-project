import { PartialType } from '@nestjs/mapped-types';
import { CreateReports2Dto } from './create-reports2.dto';

export class UpdateReports2Dto extends PartialType(CreateReports2Dto) {}
