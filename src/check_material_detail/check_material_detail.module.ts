import { Module } from '@nestjs/common';
import { CheckMaterialDetailService } from './check_material_detail.service';
import { CheckMaterialDetailController } from './check_material_detail.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Material } from 'src/materials/entities/material.entity';
import { CheckMaterial } from 'src/check_material/entities/check_material.entity';
import { CheckMaterialDetail } from './entities/check_material_detail.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([CheckMaterialDetail, CheckMaterial, Material]),
  ],
  controllers: [CheckMaterialDetailController],
  providers: [CheckMaterialDetailService],
})
export class CheckMaterialDetailModule {}
