import { IsNotEmpty } from 'class-validator';

export class CreateSalaryDto {
  @IsNotEmpty()
  datess: number;

  @IsNotEmpty()
  salary: number;

  @IsNotEmpty()
  workhour: number;
}
