import { IsNotEmpty, IsEmail, Length } from 'class-validator';
export class CreateEmployeeDto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  house_no: string;

  @IsNotEmpty()
  village_no: string;

  @IsNotEmpty()
  sub_district: string;

  @IsNotEmpty()
  district: string;

  @IsNotEmpty()
  province: string;

  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  tel: string;

  @IsNotEmpty()
  position: string;

  @IsNotEmpty()
  hourly_wage: number;

  @IsNotEmpty()
  userId: number;

  // @IsNotEmpty()
  // orderId: number;

  // @IsNotEmpty()
  // checkTimeId: number;

  image = 'No_Image_Available.jpg';
}
