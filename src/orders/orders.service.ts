import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customer } from 'src/customer/entities/customer.entity';
import { Repository } from 'typeorm';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Order } from './entities/order.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Store } from 'src/stores/entities/store.entity';
import { User } from 'src/users/entities/user.entity';
import { OrderItem } from 'src/orders/entities/order-item.entity';
import { Product } from 'src/products/entities/product.entity';

@Injectable()
export class OrdersService {
  constructor(
    @InjectRepository(Order)
    private ordersRepository: Repository<Order>,
    // @InjectRepository(Customer)
    // private customersRepository: Repository<Customer>,
    @InjectRepository(OrderItem)
    private orderItemsRepository: Repository<OrderItem>,
    // @InjectRepository(Employee)
    // private employeeRepository: Repository<Employee>,
    @InjectRepository(User)
    private userRepository: Repository<User>,
    @InjectRepository(Product)
    private productsRepository: Repository<Product>,
    @InjectRepository(Store)
    private storesRepository: Repository<Store>,
  ) {}

  async create(createOrderDto: CreateOrderDto) {
    const stores = await this.storesRepository.findOneBy({
      id: createOrderDto.stores,
    });
    const user = await this.userRepository.findOneBy({
      id: createOrderDto.userId,
    });
    const order: Order = new Order();
    order.queue = createOrderDto.queue;
    order.date = new Date();
    order.time = createOrderDto.time;

    order.discount = createOrderDto.discount;
    order.amount = createOrderDto.amount;
    order.total = createOrderDto.total;
    order.received = createOrderDto.received;
    order.change = createOrderDto.change;
    order.payment = createOrderDto.payment;
    order.stores = stores;
    order.userId = user;
    await this.ordersRepository.save(order); // ได้ id

    for (const od of createOrderDto.orderItemId) {
      const orderItem = new OrderItem();
      orderItem.name = od.name;
      // orderItem.discount = od.discount;
      orderItem.price = od.price;
      orderItem.amount = od.amount;
      orderItem.total = od.total;

      orderItem.order = order;
      orderItem.product = await this.productsRepository.findOneBy({
        id: od.product,
      });
      await this.orderItemsRepository.save(orderItem);
      order.amount = order.amount;
      order.total = order.total;
    }
    await this.ordersRepository.save(order); // ได้ id
    return await this.ordersRepository.findOne({
      where: { id: order.id },
      relations: ['orderItemId'],
    });
  }

  findAll() {
    return this.ordersRepository.find({
      relations: ['orderItemId', 'userId'],
    });
  }

  async findOne(id: number) {
    const order = await this.ordersRepository.findOne({
      where: { id: id },
      relations: ['userId', 'orderItemId'],
    });
    if (!order) {
      throw new NotFoundException();
    }
    return order;
  }

  update(id: number, updateOrderDto: UpdateOrderDto) {
    return 'This action updates a #${id} order';
  }

  async remove(id: number) {
    const order = await this.ordersRepository.findOne({
      where: { id: id },
    });
    if (!order) {
      throw new NotFoundException();
    }
    return this.ordersRepository.softRemove(order);
  }
}
