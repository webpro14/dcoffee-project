import { Injectable, NotFoundException } from '@nestjs/common';
import { Material } from 'src/materials/entities/material.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CheckMaterialDetail } from './entities/check_material_detail.entity';
import { CheckMaterial } from 'src/check_material/entities/check_material.entity';
import { CreateCheckMaterialDetailDto } from './dto/create-check_material_detail.dto';
import { UpdateCheckMaterialDetailDto } from './dto/update-check_material_detail.dto';

@Injectable()
export class CheckMaterialDetailService {
  constructor(
    @InjectRepository(CheckMaterialDetail)
    private cmdsRepository: Repository<CheckMaterialDetail>,
    @InjectRepository(CheckMaterial)
    private check_materialsRepository: Repository<CheckMaterial>,
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
  ) {}

  async create(createCheckMaterialDetailDto: CreateCheckMaterialDetailDto) {
    const check_mat = await this.check_materialsRepository.findOneBy({
      id: createCheckMaterialDetailDto.checkMaterialId,
    });
    const material = await this.materialRepository.findOneBy({
      id: createCheckMaterialDetailDto.matId,
    });
    const cmd: CheckMaterialDetail = new CheckMaterialDetail();
    cmd.check_mat = check_mat;
    cmd.material = material;
    cmd.cmd_name = createCheckMaterialDetailDto.cmd_name;
    cmd.cmd_qty_last = createCheckMaterialDetailDto.cmd_qty_last;
    cmd.cmd_qty_remain = createCheckMaterialDetailDto.cmd_qty_remain;
    cmd.cmd_qty_expire = createCheckMaterialDetailDto.cmd_qty_expire;
    await this.cmdsRepository.save(cmd);
    return await this.cmdsRepository.findOne({
      where: { id: cmd.id },
      relations: ['check_mat', 'material'],
    });
  }

  findAll() {
    return this.cmdsRepository.find({
      relations: ['check_mat', 'material'],
    });
  }

  async findOne(id: number) {
    const cmd = await this.cmdsRepository.findOne({
      where: { id: id },
      relations: ['check_mat', 'material'],
    });
    if (!cmd) {
      throw new NotFoundException();
    }
    return cmd;
  }

  async update(
    id: number,
    updateCheckMaterialDetailDto: UpdateCheckMaterialDetailDto,
  ) {
    const cmd = await this.cmdsRepository.findOne({
      where: { id: id },
    });
    if (!cmd) {
      throw new NotFoundException();
    }
    const updatedcmd = { ...cmd, ...updateCheckMaterialDetailDto };
    return this.cmdsRepository.save(updatedcmd);
  }

  async remove(id: number) {
    const cmd = await this.cmdsRepository.findOne({
      where: { id: id },
    });
    if (!cmd) {
      throw new NotFoundException();
    }
    return this.cmdsRepository.softRemove(cmd);
  }
}
