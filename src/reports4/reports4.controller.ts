import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseInterceptors,
  UploadedFile,
  Res,
  Request,
} from '@nestjs/common';
import { Reports4Service } from './reports4.service';
import { CreateReports4Dto } from './dto/create-reports4.dto';
import { UpdateReports4Dto } from './dto/update-reports4.dto';
import { diskStorage } from 'multer';
import { FileInterceptor } from '@nestjs/platform-express/multer';
import { extname } from 'path';
import { v4 as uuidv4 } from 'uuid';
import { Response } from 'express';

@Controller('reports4')
export class Reports4Controller {
  constructor(private readonly reports4Service: Reports4Service) {}

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './product_images',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createReports4Dto: CreateReports4Dto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    createReports4Dto.image = file.filename;
    return this.reports4Service.create(createReports4Dto);
  }

  @Get('/product')
  getProduct(
    @Query()
    query: {
      searchText?: string;
      upperPrice?: number;
      lowPrice?: number;
    },
  ) {
    console.log(query);
    return this.reports4Service.getProduct();
  }

  @Get()
  findAll(@Query() query, @Request() req: any) {
    return this.reports4Service.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reports4Service.findOne(+id);
  }

  @Get(':id/image')
  async getImage(@Param('id') id: string, @Res() res: Response) {
    const reports2 = await this.reports4Service.findOne(+id);
    res.sendFile(reports2.image, { root: './employee_images' });
  }

  @Patch(':id/image')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './product_images',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  updateImage(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
  ) {
    return this.reports4Service.update(+id, { image: file.filename });
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateProductDto: UpdateReports4Dto) {
    return this.reports4Service.update(+id, updateProductDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reports4Service.remove(+id);
  }
}
