import { Product } from 'src/products/entities/product.entity';
import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
} from 'typeorm';
import { Order } from './order.entity';
@Entity()
export class OrderItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  // @Column({ default: 0 })
  // discount: number;

  @Column({ default: 0 })
  price: number;

  @Column({ default: 0 })
  amount: number;

  @Column({ default: 0 })
  total: number;

  @ManyToOne(() => Order, (order) => order.orderItemId)
  order: Order;
  @ManyToOne(() => Product, (product) => product.orderItem)
  product: Product; // Product Id

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updatedDate: Date;

  @DeleteDateColumn()
  deletedDate: Date;
}
