import { IsNotEmpty } from 'class-validator';
export class CreateBillDto {
  @IsNotEmpty()
  bill_shop_name: string;

  @IsNotEmpty()
  bill_date: Date;

  @IsNotEmpty()
  bill_time: string;

  @IsNotEmpty()
  bill_total: number;

  @IsNotEmpty()
  bill_buy: number;

  @IsNotEmpty()
  bill_change: number;

  @IsNotEmpty()
  employeeId: number;
}
