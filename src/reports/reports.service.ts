import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { CreateReportDto } from './dto/create-report.dto';
import { UpdateReportDto } from './dto/update-report.dto';
import { report } from './entities/report.entity';

@Injectable()
export class ReportsService {
  constructor(
    @InjectRepository(report)
    private reportsRepository: Repository<report>,
    @InjectDataSource() private dataSource: DataSource,
  ) {}

  getCustomer() {
    return this.dataSource.query('CALL `getCustomer`();');
  }

  create(createReportDto: CreateReportDto) {
    return this.reportsRepository.save(createReportDto);
  }

  findAll() {
    return this.reportsRepository.find();
  }

  findOne(id: number) {
    return this.reportsRepository.findOne({
      where: { id: id },
      relations: ['orders'],
    });
  }

  async update(id: number, updateReportDto: UpdateReportDto) {
    const report = await this.reportsRepository.findOneBy({ id });
    if (!report) {
      throw new NotFoundException();
    }
    const updateReport = { ...report, ...updateReportDto };

    return this.reportsRepository.save(updateReport);
  }

  async remove(id: number) {
    const report = await this.reportsRepository.findOneBy({ id });
    if (!report) {
      throw new NotFoundException();
    }
    return this.reportsRepository.remove(report);
  }
}
