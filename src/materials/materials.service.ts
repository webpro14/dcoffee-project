import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMaterialDto } from './dto/create-material.dto';
import { UpdateMaterialDto } from './dto/update-material.dto';
import { Material } from './entities/material.entity';

@Injectable()
export class MaterialsService {
  constructor(
    @InjectRepository(Material)
    private materialsRepository: Repository<Material>,
  ) {}
  create(createMaterialDto: CreateMaterialDto) {
    return this.materialsRepository.save(createMaterialDto);
  }

  findAll() {
    return this.materialsRepository.find();
  }

  findOne(id: number) {
    return this.materialsRepository.findOne({ where: { id } });
  }

  async update(id: number, updateMaterialDto: UpdateMaterialDto) {
    const material = await this.materialsRepository.findOneBy({ id });
    if (!material) {
      throw new NotFoundException();
    }
    const updateMaterial = { ...material, ...updateMaterialDto };

    return this.materialsRepository.save(updateMaterial);
  }

  async remove(id: number) {
    const material = await this.materialsRepository.findOneBy({ id });
    if (!material) {
      throw new NotFoundException();
    }
    return this.materialsRepository.softRemove(material);
  }
}
