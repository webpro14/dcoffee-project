import { PartialType } from '@nestjs/mapped-types';
import { CreateCheckTimeDto } from './create-check-time.dto';

export class UpdateCheckTimeDto extends PartialType(CreateCheckTimeDto) {}
