import { Test, TestingModule } from '@nestjs/testing';
import { Reports4Service } from './reports4.service';

describe('Reports4Service', () => {
  let service: Reports4Service;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Reports4Service],
    }).compile();

    service = module.get<Reports4Service>(Reports4Service);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
