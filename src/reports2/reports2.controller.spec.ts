import { Test, TestingModule } from '@nestjs/testing';
import { Reports2Controller } from './reports2.controller';
import { Reports2Service } from './reports2.service';

describe('Reports2Controller', () => {
  let controller: Reports2Controller;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Reports2Controller],
      providers: [Reports2Service],
    }).compile();

    controller = module.get<Reports2Controller>(Reports2Controller);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
