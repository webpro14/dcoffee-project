import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateReports2Dto } from './dto/create-reports2.dto';
import { UpdateReports2Dto } from './dto/update-reports2.dto';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Like, Repository } from 'typeorm';
import { Reports2 } from './entities/reports2.entity';
import { User } from 'src/users/entities/user.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Category } from 'src/categorys/entities/category.entity';

@Injectable()
export class Reports2Service {
  constructor(
    @InjectRepository(Reports2)
    private reports2Repository: Repository<Reports2>,
    @InjectRepository(Category)
    private categorysRepository: Repository<Category>,
    @InjectDataSource() private dataSource: DataSource,
  ) {}

  getProduct() {
    return this.dataSource.query('SELECT * FROM getProduct_info01');
  }

  async create(createReports2Dto: CreateReports2Dto) {
    const category = await this.categorysRepository.findOneBy({
      id: createReports2Dto.categoryId,
    });
    const reports2: Reports2 = new Reports2();
    reports2.category = category;
    reports2.name = createReports2Dto.name;
    reports2.type = createReports2Dto.type;
    reports2.size = createReports2Dto.size;
    reports2.price = createReports2Dto.price;
    reports2.image = createReports2Dto.image;

    await this.reports2Repository.save(reports2);
    return await this.reports2Repository.findOne({
      where: { id: reports2.id },
      relations: ['category'],
    });
  }

  findByCategory(id: number) {
    return this.reports2Repository.find({ where: { categoryId: id } });
  }

  async findAll(query): Promise<Paginate> {
    const cat = query.cat || 1;
    const page = query.page || 1;
    const take = query.take || 10;
    const skip = (page - 1) * take;
    const keyword = query.keyword || '';
    const orderBy = query.orderBy || 'name';
    const order = query.order || 'ASC';
    const currentPage = page;

    const [result, total] = await this.reports2Repository.findAndCount({
      relations: ['category'],
      where: { name: Like(`%${keyword}%`) },
      order: { [orderBy]: order },
    });
    const lastPage = Math.ceil(total / take);
    return {
      data: result,
      count: total,
      currentPage: currentPage,
      lastPage: lastPage,
    };
  }

  findOne(id: number) {
    return this.reports2Repository.findOne({
      where: { id: id },
    });
  }

  async update(id: number, updateReports2Dto: UpdateReports2Dto) {
    try {
      const updatedReports2 = await this.reports2Repository.save({
        id,
        ...updateReports2Dto,
      });
      return updatedReports2;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const reports2 = await this.reports2Repository.findOne({
      where: { id: id },
    });
    try {
      const deletedReports2 = await this.reports2Repository.remove(reports2);
      return deletedReports2;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
