import { Module } from '@nestjs/common';
import { Reports3Service } from './reports3.service';
import { Reports3Controller } from './reports3.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reports3 } from './entities/reports3.entity';

import { Category } from 'src/categorys/entities/category.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Reports3, Category])],
  controllers: [Reports3Controller],
  providers: [Reports3Service],
  exports: [Reports3Service],
})
export class Reports3Module {}
