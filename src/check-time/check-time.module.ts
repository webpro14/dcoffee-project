import { Module } from '@nestjs/common';
import { CheckTimeService } from './check-time.service';
import { CheckTimeController } from './check-time.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Employee } from 'src/employees/entities/employee.entity';
import { Salary } from 'src/salarys/entities/salary.entity';
import { CheckTime } from './entities/check-time.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Employee, Salary, CheckTime])],
  controllers: [CheckTimeController],
  providers: [CheckTimeService],
  exports: [CheckTimeService],
})
export class CheckTimeModule {}
