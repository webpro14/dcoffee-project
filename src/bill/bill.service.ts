import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateBillDto } from './dto/create-bill.dto';
import { UpdateBillDto } from './dto/update-bill.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Bill } from './entities/bill.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Repository } from 'typeorm';

@Injectable()
export class BillService {
  constructor(
    @InjectRepository(Bill)
    private billsRepository: Repository<Bill>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {}

  async create(createBillDto: CreateBillDto) {
    const employee = await this.employeeRepository.findOneBy({
      id: createBillDto.employeeId,
    });
    const bill: Bill = new Bill();
    bill.employee = employee;
    bill.bill_shop_name = createBillDto.bill_shop_name;
    bill.bill_date = createBillDto.bill_date;
    bill.bill_time = createBillDto.bill_time;
    bill.bill_total = createBillDto.bill_total;
    bill.bill_buy = createBillDto.bill_buy;
    bill.bill_change = createBillDto.bill_change;
    await this.billsRepository.save(bill);
    return await this.billsRepository.findOne({
      where: { id: bill.id },
      relations: ['employee'],
    });
  }

  findAll() {
    return this.billsRepository.find({ relations: ['employee'] });
  }

  async findOne(id: number) {
    const bill = await this.billsRepository.findOne({
      where: { id: id },
      relations: ['employee'],
    });
    if (!bill) {
      throw new NotFoundException();
    }
    return bill;
  }

  async update(id: number, updateBillDto: UpdateBillDto) {
    const bill = await this.billsRepository.findOne({
      where: { id: id },
    });
    if (!bill) {
      throw new NotFoundException();
    }
    const updatedbill = { ...bill, ...updateBillDto };
    return this.billsRepository.save(updatedbill);
  }

  async remove(id: number) {
    const bill = await this.billsRepository.findOne({
      where: { id: id },
    });
    if (!bill) {
      throw new NotFoundException();
    }
    return this.billsRepository.softRemove(bill);
  }
}
