import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCategoryDto } from './dto/create-category.dto';
import { UpdateCategoryDto } from './dto/update-category.dto';
import { Category } from './entities/category.entity';

@Injectable()
export class CategorysService {
  constructor(
    @InjectRepository(Category)
    private categorysRepository: Repository<Category>,
  ) {}

  create(createCategoryDto: CreateCategoryDto) {
    return this.categorysRepository.save(createCategoryDto);
  }

  findAll() {
    return this.categorysRepository.find({});
  }

  findOne(id: number) {
    return this.categorysRepository.findOne({
      where: { id: id },
    });
  }

  async update(id: number, updateCategoryDto: UpdateCategoryDto) {
    const category = await this.categorysRepository.findOneBy({ id: id });
    if (!category) {
      throw new NotFoundException();
    }
    const updateCategory = { ...category, ...updateCategoryDto };

    return this.categorysRepository.save(updateCategory);
  }

  async remove(id: number) {
    const category = await this.categorysRepository.findOneBy({ id: id });
    if (!category) {
      throw new NotFoundException();
    }
    return this.categorysRepository.softRemove(category);
  }
}
