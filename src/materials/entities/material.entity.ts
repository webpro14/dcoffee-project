import { BillDetail } from 'src/bill_detail/entities/bill_detail.entity';

import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Material {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: '32' })
  name: string;

  @Column()
  min_qty: number;

  @Column()
  qty: number;

  @Column({ length: '32' })
  unit: string;

  @Column({ type: 'float' })
  price_per_unit: number;

  @OneToMany(() => BillDetail, (bill_detail) => bill_detail.material)
  bill_detail: BillDetail;

  @OneToMany(
    () => BillDetail,
    (check_material_detail) => check_material_detail.material,
  )
  check_material_detail: BillDetail;

  @CreateDateColumn()
  createAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;

  // @ManyToOne(() => Detail, (material) => detail.material)
  // detail: Detail;
}
