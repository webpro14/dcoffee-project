import { Module } from '@nestjs/common';
import { Reports4Service } from './reports4.service';
import { Reports4Controller } from './reports4.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Reports4 } from './entities/reports4.entity';

import { Category } from 'src/categorys/entities/category.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Reports4, Category])],
  controllers: [Reports4Controller],
  providers: [Reports4Service],
  exports: [Reports4Service],
})
export class Reports4Module {}
