import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { CustomerModule } from './customer/customer.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from './products/entities/product.entity';
import { DataSource } from 'typeorm';
import { Customer } from './customer/entities/customer.entity';
import { UsersModule } from './users/users.module';
import { User } from './users/entities/user.entity';
import { AuthModule } from './auth/auth.module';
import { EmployeesModule } from './employees/employees.module';
import { Employee } from './employees/entities/employee.entity';
import { ProductsModule } from './products/products.module';
import { StoresModule } from './stores/stores.module';
import { Store } from './stores/entities/store.entity';
import { MaterialsModule } from './materials/materials.module';
import { Material } from './materials/entities/material.entity';
import { Order } from './orders/entities/order.entity';

import { OrdersModule } from './orders/orders.module';
import { Category } from './categorys/entities/category.entity';
import { CategorysModule } from './categorys/categorys.module';
import { ReportsModule } from './reports/reports.module';
import { Reports2Module } from './reports2/reports2.module';
import { SalarysModule } from './salarys/salarys.module';
import { CheckTimeModule } from './check-time/check-time.module';
import { CheckTime } from './check-time/entities/check-time.entity';
import { Salary } from './salarys/entities/salary.entity';
import { BillModule } from './bill/bill.module';
import { Bill } from './bill/entities/bill.entity';
import { BillDetailModule } from './bill_detail/bill_detail.module';
import { BillDetail } from './bill_detail/entities/bill_detail.entity';
import { CheckMaterialModule } from './check_material/check_material.module';
import { CheckMaterialDetailModule } from './check_material_detail/check_material_detail.module';
import { CheckMaterial } from './check_material/entities/check_material.entity';
import { CheckMaterialDetail } from './check_material_detail/entities/check_material_detail.entity';
import { report } from './reports/entities/report.entity';
import { Reports2 } from './reports2/entities/reports2.entity';

// import { ScheduleModule } from '@nestjs/schedule';

import { Reports3 } from './reports3/entities/reports3.entity';
import { Reports3Module } from './reports3/reports3.module';
import { Reports4Module } from './reports4/reports4.module';
import { Reports4 } from './reports4/entities/reports4.entity';
import { OrderItem } from './orders/entities/order-item.entity';

@Module({
  imports: [
    // ScheduleModule.forRoot(),
    TypeOrmModule.forRoot({
      // type: 'sqlite',
      // database: 'db.sqlite',
      // entities: [
      //   Customer,
      //   User,
      //   Employee,
      //   Product,
      //   Store,
      //   Material,
      //   Order,
      //   OrderItem,
      //   Category,
      // ],
      // synchronize: true,
      // migrations: [],
      type: 'mysql',
      host: 'db4free.net',
      port: 3306,
      username: 'db_coffee',
      password: 'pass@1234',
      database: 'db_coffee',
      entities: [
        Customer,
        User,
        Employee,
        Product,
        Store,
        Material,
        Order,
        OrderItem,
        Category,
        CheckTime,
        Salary,
        Bill,
        BillDetail,
        CheckMaterial,
        CheckMaterialDetail,
        report,
        Reports2,
        Reports3,
        Reports4,
      ],
      synchronize: true,
    }),

    CustomerModule,
    UsersModule,
    AuthModule,
    EmployeesModule,
    ProductsModule,
    StoresModule,
    MaterialsModule,
    OrdersModule,
    CategorysModule,
    ReportsModule,
    Reports2Module,
    ReportsModule,
    Reports3Module,
    CheckTimeModule,
    BillModule,
    BillDetailModule,
    CheckMaterialModule,
    CheckMaterialDetailModule,
    SalarysModule,
    Reports4Module,
  ],

  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
