import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
  UseInterceptors,
  UploadedFile,
  Res,
  Request,
} from '@nestjs/common';
import { Reports3Service } from './reports3.service';
import { CreateReports3Dto } from './dto/create-reports3.dto';
import { UpdateReports3Dto } from './dto/update-reports3.dto';
import { diskStorage } from 'multer';
import { FileInterceptor } from '@nestjs/platform-express/multer';
import { extname } from 'path';
import { v4 as uuidv4 } from 'uuid';
import { Response } from 'express';

@Controller('reports3')
export class Reports3Controller {
  constructor(private readonly reports3Service: Reports3Service) {}

  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './product_images',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  create(
    @Body() createReports3Dto: CreateReports3Dto,
    @UploadedFile() file: Express.Multer.File,
  ) {
    createReports3Dto.image = file.filename;
    return this.reports3Service.create(createReports3Dto);
  }

  @Get('/product')
  getProduct(
    @Query()
    query: {
      searchText?: string;
      upperPrice?: number;
      lowPrice?: number;
    },
  ) {
    console.log(query);
    return this.reports3Service.getProduct();
  }

  @Get()
  findAll(@Query() query, @Request() req: any) {
    return this.reports3Service.findAll(query);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.reports3Service.findOne(+id);
  }

  @Get(':id/image')
  async getImage(@Param('id') id: string, @Res() res: Response) {
    const reports2 = await this.reports3Service.findOne(+id);
    res.sendFile(reports2.image, { root: './employee_images' });
  }

  @Patch(':id/image')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './product_images',
        filename: (req, file, cb) => {
          const name = uuidv4();
          return cb(null, name + extname(file.originalname));
        },
      }),
    }),
  )
  updateImage(
    @Param('id') id: string,
    @UploadedFile() file: Express.Multer.File,
  ) {
    return this.reports3Service.update(+id, { image: file.filename });
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateProductDto: UpdateReports3Dto) {
    return this.reports3Service.update(+id, updateProductDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.reports3Service.remove(+id);
  }
}
