import { IsNotEmpty, MinLength } from 'class-validator';
export class CreateReportDto {
  @IsNotEmpty()
  @MinLength(2)
  name: string;

  @IsNotEmpty()
  @MinLength(10)
  tel: string;

  @IsNotEmpty()
  point: string;
}
