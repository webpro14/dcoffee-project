import { IsNotEmpty } from 'class-validator';
export class CreateCheckMaterialDto {
  @IsNotEmpty()
  check_mat_date: Date;

  @IsNotEmpty()
  check_mat_time: string;

  @IsNotEmpty()
  employeeId: number;
}
