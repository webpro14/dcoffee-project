import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { Salary } from './entities/salary.entity';

@Injectable()
export class SalarysService {
  constructor(
    @InjectRepository(Salary)
    private salarysRepository: Repository<Salary>,
  ) {}
  create(CreateSalaryDto: CreateSalaryDto) {
    return this.salarysRepository.save(CreateSalaryDto);
  }

  findAll() {
    return this.salarysRepository.find({});
  }

  findOne(id: number) {
    return this.salarysRepository.findOne({
      where: { id: id },
    });
  }

  async update(id: number, UpdateSalaryDto: UpdateSalaryDto) {
    const salary = await this.salarysRepository.findOneBy({ id: id });
    if (!salary) {
      throw new NotFoundException();
    }
    const updateSalary = { ...salary, ...UpdateSalaryDto };

    return this.salarysRepository.save(updateSalary);
  }

  async remove(id: number) {
    const salary = await this.salarysRepository.findOneBy({ id: id });
    if (!salary) {
      throw new NotFoundException();
    }
    return this.salarysRepository.softRemove(salary);
  }
}
