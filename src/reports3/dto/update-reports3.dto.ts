import { PartialType } from '@nestjs/mapped-types';
import { CreateReports3Dto } from './create-reports3.dto';

export class UpdateReports3Dto extends PartialType(CreateReports3Dto) {}
