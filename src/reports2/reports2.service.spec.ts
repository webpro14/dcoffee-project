import { Test, TestingModule } from '@nestjs/testing';
import { Reports2Service } from './reports2.service';

describe('Reports2Service', () => {
  let service: Reports2Service;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Reports2Service],
    }).compile();

    service = module.get<Reports2Service>(Reports2Service);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
