import { BillDetail } from 'src/bill_detail/entities/bill_detail.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Bill {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  bill_shop_name: string;

  @Column()
  bill_date: Date;

  @Column()
  bill_time: string;

  @Column()
  bill_total: number;

  @Column()
  bill_buy: number;

  @Column()
  bill_change: number;

  @ManyToOne(() => Employee, (employee) => employee.bill)
  employee: Employee;

  @OneToMany(() => BillDetail, (bill_detail) => bill_detail.bill)
  bill_detail: BillDetail;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updateDate: Date;

  @DeleteDateColumn()
  deleteDate: Date;
}
