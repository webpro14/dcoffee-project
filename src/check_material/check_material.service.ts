import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateCheckMaterialDto } from './dto/create-check_material.dto';
import { UpdateCheckMaterialDto } from './dto/update-check_material.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { CheckMaterial } from './entities/check_material.entity';
import { Employee } from 'src/employees/entities/employee.entity';
import { Repository } from 'typeorm';

@Injectable()
export class CheckMaterialService {
  constructor(
    @InjectRepository(CheckMaterial)
    private check_materialsRepository: Repository<CheckMaterial>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
  ) {}

  async create(createCheckMaterialDto: CreateCheckMaterialDto) {
    const employee = await this.employeeRepository.findOneBy({
      id: createCheckMaterialDto.employeeId,
    });
    const check_material: CheckMaterial = new CheckMaterial();
    check_material.employee = employee;
    check_material.check_mat_date = createCheckMaterialDto.check_mat_date;
    check_material.check_mat_time = createCheckMaterialDto.check_mat_time;
    await this.check_materialsRepository.save(check_material);
    return await this.check_materialsRepository.findOne({
      where: { id: check_material.id },
      relations: ['employee'],
    });
  }

  findAll() {
    return this.check_materialsRepository.find({ relations: ['employee'] });
  }

  async findOne(id: number) {
    const check_material = await this.check_materialsRepository.findOne({
      where: { id: id },
      relations: ['employee'],
    });
    if (!check_material) {
      throw new NotFoundException();
    }
    return check_material;
  }

  async update(id: number, updateCheckMaterialDto: UpdateCheckMaterialDto) {
    const check_material = await this.check_materialsRepository.findOne({
      where: { id: id },
    });
    if (!check_material) {
      throw new NotFoundException();
    }
    const updatedcheck_material = {
      ...check_material,
      ...updateCheckMaterialDto,
    };
    return this.check_materialsRepository.save(updatedcheck_material);
  }

  async remove(id: number) {
    const check_material = await this.check_materialsRepository.findOne({
      where: { id: id },
    });
    if (!check_material) {
      throw new NotFoundException();
    }
    return this.check_materialsRepository.softRemove(check_material);
  }
}
