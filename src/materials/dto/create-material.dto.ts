import { IsNotEmpty, Length } from 'class-validator';

export class CreateMaterialDto {
  @IsNotEmpty()
  @Length(2, 64)
  name: string;

  @IsNotEmpty()
  min_qty: number;

  @IsNotEmpty()
  qty: number;

  @IsNotEmpty()
  @Length(3, 64)
  unit: string;

  @IsNotEmpty()
  price_per_unit: number;
}
