import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
import { CheckTimeService } from './check-time.service';
import { CreateCheckTimeDto } from './dto/create-check-time.dto';
import { UpdateCheckTimeDto } from './dto/update-check-time.dto';

@Controller('check-time')
export class CheckTimeController {
  constructor(private readonly checkTimeService: CheckTimeService) {}

  @Post()
  create(@Body() createCheckTimeDto: CreateCheckTimeDto) {
    return this.checkTimeService.create(createCheckTimeDto);
  }

  @Get()
  findAll() {
    return this.checkTimeService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.checkTimeService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateCheckTimeDto: UpdateCheckTimeDto,
  ) {
    return this.checkTimeService.update(+id, updateCheckTimeDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.checkTimeService.remove(+id);
  }
}
