import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateReports3Dto } from './dto/create-reports3.dto';
import { UpdateReports3Dto } from './dto/update-reports3.dto';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Like, Repository } from 'typeorm';
import { Reports3 } from './entities/reports3.entity';
import { User } from 'src/users/entities/user.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Category } from 'src/categorys/entities/category.entity';

@Injectable()
export class Reports3Service {
  constructor(
    @InjectRepository(Reports3)
    private reports3Repository: Repository<Reports3>,
    @InjectRepository(Category)
    private categorysRepository: Repository<Category>,
    @InjectDataSource() private dataSource: DataSource,
  ) {}

  getProduct() {
    return this.dataSource.query('SELECT * FROM getProduct_info02');
  }

  async create(createReports3Dto: CreateReports3Dto) {
    const category = await this.categorysRepository.findOneBy({
      id: createReports3Dto.categoryId,
    });
    const reports2: Reports3 = new Reports3();
    reports2.category = category;
    reports2.name = createReports3Dto.name;
    reports2.type = createReports3Dto.type;
    reports2.size = createReports3Dto.size;
    reports2.price = createReports3Dto.price;
    reports2.image = createReports3Dto.image;

    await this.reports3Repository.save(reports2);
    return await this.reports3Repository.findOne({
      where: { id: reports2.id },
      relations: ['category'],
    });
  }

  findByCategory(id: number) {
    return this.reports3Repository.find({ where: { categoryId: id } });
  }

  async findAll(query): Promise<Paginate> {
    const cat = query.cat || 1;
    const page = query.page || 1;
    const take = query.take || 10;
    const skip = (page - 1) * take;
    const keyword = query.keyword || '';
    const orderBy = query.orderBy || 'name';
    const order = query.order || 'ASC';
    const currentPage = page;

    const [result, total] = await this.reports3Repository.findAndCount({
      relations: ['category'],
      where: { name: Like(`%${keyword}%`) },
      order: { [orderBy]: order },
    });
    const lastPage = Math.ceil(total / take);
    return {
      data: result,
      count: total,
      currentPage: currentPage,
      lastPage: lastPage,
    };
  }

  findOne(id: number) {
    return this.reports3Repository.findOne({
      where: { id: id },
    });
  }

  async update(id: number, updateReports3Dto: UpdateReports3Dto) {
    try {
      const updatedReports2 = await this.reports3Repository.save({
        id,
        ...updateReports3Dto,
      });
      return updatedReports2;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const reports2 = await this.reports3Repository.findOne({
      where: { id: id },
    });
    try {
      const deletedReports2 = await this.reports3Repository.remove(reports2);
      return deletedReports2;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
