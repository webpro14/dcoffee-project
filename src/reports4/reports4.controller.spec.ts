import { Test, TestingModule } from '@nestjs/testing';
import { Reports4Controller } from './reports4.controller';
import { Reports4Service } from './reports4.service';

describe('Reports4Controller', () => {
  let controller: Reports4Controller;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Reports4Controller],
      providers: [Reports4Service],
    }).compile();

    controller = module.get<Reports4Controller>(Reports4Controller);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
