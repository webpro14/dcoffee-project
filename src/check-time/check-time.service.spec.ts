import { Test, TestingModule } from '@nestjs/testing';
import { CheckTimeService } from './check-time.service';

describe('CheckTimeService', () => {
  let service: CheckTimeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckTimeService],
    }).compile();

    service = module.get<CheckTimeService>(CheckTimeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
