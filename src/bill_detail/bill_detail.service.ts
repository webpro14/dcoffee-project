import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { BillDetail } from './entities/bill_detail.entity';
import { Repository } from 'typeorm';
import { Bill } from 'src/bill/entities/bill.entity';
import { CreateBillDetailDto } from './dto/create-bill_detail.dto';
import { Material } from 'src/materials/entities/material.entity';
import { UpdateBillDetailDto } from './dto/update-bill_detail.dto';

@Injectable()
export class BillDetailService {
  constructor(
    @InjectRepository(BillDetail)
    private bill_detailsRepository: Repository<BillDetail>,
    @InjectRepository(Bill)
    private billRepository: Repository<Bill>,
    @InjectRepository(Material)
    private materialRepository: Repository<Material>,
  ) {}

  async create(createBillDetailDto: CreateBillDetailDto) {
    const bill = await this.billRepository.findOneBy({
      id: createBillDetailDto.billId,
    });
    const material = await this.materialRepository.findOneBy({
      id: createBillDetailDto.matId,
    });
    const bill_detail: BillDetail = new BillDetail();
    bill_detail.bill = bill;
    bill_detail.material = material;
    bill_detail.bill_detail_name = createBillDetailDto.bill_detail_name;
    bill_detail.bill_detail_amount = createBillDetailDto.bill_detail_amount;
    bill_detail.bill_detail_price = createBillDetailDto.bill_detail_price;
    bill_detail.bill_detail_total = createBillDetailDto.bill_detail_total;
    await this.bill_detailsRepository.save(bill_detail);
    return await this.bill_detailsRepository.findOne({
      where: { id: bill_detail.id },
      relations: ['bill', 'material'],
    });
  }

  findAll() {
    return this.bill_detailsRepository.find({
      relations: ['bill', 'material'],
    });
  }

  async findOne(id: number) {
    const bill_detail = await this.bill_detailsRepository.findOne({
      where: { id: id },
      relations: ['bill', 'material'],
    });
    if (!bill_detail) {
      throw new NotFoundException();
    }
    return bill_detail;
  }

  async update(id: number, updateBillDetailDto: UpdateBillDetailDto) {
    const bill_detail = await this.bill_detailsRepository.findOne({
      where: { id: id },
    });
    if (!bill_detail) {
      throw new NotFoundException();
    }
    const updatedbill_detail = { ...bill_detail, ...updateBillDetailDto };
    return this.bill_detailsRepository.save(updatedbill_detail);
  }

  async remove(id: number) {
    const bill_detail = await this.bill_detailsRepository.findOne({
      where: { id: id },
    });
    if (!bill_detail) {
      throw new NotFoundException();
    }
    return this.bill_detailsRepository.softRemove(bill_detail);
  }
}
