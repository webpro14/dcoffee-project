import { IsNotEmpty, MinLength } from 'class-validator';
export class CreateCustomerDto {
  @IsNotEmpty()
  @MinLength(2)
  name: string;

  @IsNotEmpty()
  @MinLength(10)
  tel: string;

  @IsNotEmpty()
  point: string;

  // @IsNotEmpty()
  // startDate: Date;
}
