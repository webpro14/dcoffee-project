import { Test, TestingModule } from '@nestjs/testing';
import { CheckTimeController } from './check-time.controller';
import { CheckTimeService } from './check-time.service';

describe('CheckTimeController', () => {
  let controller: CheckTimeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CheckTimeController],
      providers: [CheckTimeService],
    }).compile();

    controller = module.get<CheckTimeController>(CheckTimeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
