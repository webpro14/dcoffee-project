// import { Order } from 'src/orders/entities/order.entity';
// import { Product } from 'src/products/entities/product.entity';
// import {
//   Entity,
//   PrimaryGeneratedColumn,
//   Column,
//   CreateDateColumn,
//   UpdateDateColumn,
//   DeleteDateColumn,
//   ManyToOne,
// } from 'typeorm';

// @Entity()
// export class OrderItem {
//   @PrimaryGeneratedColumn()
//   id: number;

//   @Column({
//     length: '32',
//   })
//   name: string;

//   // @Column()
//   // discount: number;

//   @Column({ type: 'float' })
//   price: number;

//   @Column()
//   amount: number;

//   @Column({ type: 'float' })
//   total: number;

//   @ManyToOne(() => Order, (order) => order.orderItem)
//   order: Order;

//   @ManyToOne(() => Product, (product) => product.orderItem)
//   product: Product; // Product Id

//   @CreateDateColumn()
//   createdDate: Date;

//   @UpdateDateColumn()
//   updatedDate: Date;

//   @DeleteDateColumn()
//   deletedDate: Date;
// }
