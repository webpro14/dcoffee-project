import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Like } from 'typeorm';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { UpdateCustomerDto } from './dto/update-customer.dto';
import { Customer } from './entities/customer.entity';

@Injectable()
export class CustomerService {
  constructor(
    @InjectRepository(Customer)
    private customersRepository: Repository<Customer>,
  ) {}

  create(createCustomerDto: CreateCustomerDto) {
    return this.customersRepository.save(createCustomerDto);
  }

  async findAll(query): Promise<Paginate> {
    const cat = query.cat || 1;
    const page = query.page || 1;
    const take = query.take || 10;
    const skip = (page - 1) * take;
    const keyword = query.keyword || 'tel';
    // const orderBy = query.orderBy || '';
    const order = query.order || 'ASC';
    const currentPage = page;

    const [result, total] = await this.customersRepository.findAndCount({
      relations: ['orders'],
      where: { tel: Like(`%${keyword}%`) },
      // order: { [orderBy]: order },
    });
    const lastPage = Math.ceil(total / take);
    return {
      data: result,
      count: total,
      currentPage: currentPage,
      lastPage: lastPage,
    };
  }

  findOne(id: number) {
    return this.customersRepository.findOne({
      where: { id: id },
      relations: ['orders'],
    });
  }

  findOneBytel(tel: string) {
    return this.customersRepository.findOne({
      where: { tel: tel },
      relations: ['orders'],
    });
  }

  async update(id: number, updateCustomerDto: UpdateCustomerDto) {
    const customer = await this.customersRepository.findOneBy({ id });
    if (!customer) {
      throw new NotFoundException();
    }
    const updateCustomer = { ...customer, ...updateCustomerDto };
    return this.customersRepository.save(updateCustomer);
  }

  async remove(id: number) {
    const customer = await this.customersRepository.findOneBy({ id });
    if (!customer) {
      throw new NotFoundException();
    }
    return this.customersRepository.remove(customer);
  }
}
