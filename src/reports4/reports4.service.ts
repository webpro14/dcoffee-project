import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateReports4Dto } from './dto/create-reports4.dto';
import { UpdateReports4Dto } from './dto/update-reports4.dto';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Like, Repository } from 'typeorm';
import { Reports4 } from './entities/reports4.entity';
import { User } from 'src/users/entities/user.entity';
import { Order } from 'src/orders/entities/order.entity';
import { Category } from 'src/categorys/entities/category.entity';

@Injectable()
export class Reports4Service {
  constructor(
    @InjectRepository(Reports4)
    private reports4Repository: Repository<Reports4>,
    @InjectRepository(Category)
    private categorysRepository: Repository<Category>,
    @InjectDataSource() private dataSource: DataSource,
  ) {}

  getProduct() {
    return this.dataSource.query('SELECT * FROM getProduct_info03');
  }

  async create(createReports4Dto: CreateReports4Dto) {
    const category = await this.categorysRepository.findOneBy({
      id: createReports4Dto.categoryId,
    });
    const reports2: Reports4 = new Reports4();
    reports2.category = category;
    reports2.name = createReports4Dto.name;
    reports2.type = createReports4Dto.type;
    reports2.size = createReports4Dto.size;
    reports2.price = createReports4Dto.price;
    reports2.image = createReports4Dto.image;

    await this.reports4Repository.save(reports2);
    return await this.reports4Repository.findOne({
      where: { id: reports2.id },
      relations: ['category'],
    });
  }

  findByCategory(id: number) {
    return this.reports4Repository.find({ where: { categoryId: id } });
  }

  async findAll(query): Promise<Paginate> {
    const cat = query.cat || 1;
    const page = query.page || 1;
    const take = query.take || 10;
    const skip = (page - 1) * take;
    const keyword = query.keyword || '';
    const orderBy = query.orderBy || 'name';
    const order = query.order || 'ASC';
    const currentPage = page;

    const [result, total] = await this.reports4Repository.findAndCount({
      relations: ['category'],
      where: { name: Like(`%${keyword}%`) },
      order: { [orderBy]: order },
    });
    const lastPage = Math.ceil(total / take);
    return {
      data: result,
      count: total,
      currentPage: currentPage,
      lastPage: lastPage,
    };
  }

  findOne(id: number) {
    return this.reports4Repository.findOne({
      where: { id: id },
    });
  }

  async update(id: number, updateReports4Dto: UpdateReports4Dto) {
    try {
      const updatedReports4 = await this.reports4Repository.save({
        id,
        ...updateReports4Dto,
      });
      return updatedReports4;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const reports2 = await this.reports4Repository.findOne({
      where: { id: id },
    });
    try {
      const deletedReports2 = await this.reports4Repository.remove(reports2);
      return deletedReports2;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
