import { IsNotEmpty, Length } from 'class-validator';

export class CreateReports2Dto {
  @IsNotEmpty()
  @Length(3, 32)
  name: string;

  @IsNotEmpty()
  type: string;

  @IsNotEmpty()
  size: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  categoryId: number;

  image = 'No_Image_Available.jpg';
}
