import { CheckMaterial } from 'src/check_material/entities/check_material.entity';
import { Material } from 'src/materials/entities/material.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class CheckMaterialDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  cmd_name: string;

  @Column()
  cmd_qty_last: number;

  @Column()
  cmd_qty_remain: number;

  @Column()
  cmd_qty_expire: number;

  @ManyToOne(() => Material, (material) => material.check_material_detail)
  material: Material;

  @ManyToOne(
    () => CheckMaterial,
    (check_mat) => check_mat.check_material_detail,
  )
  check_mat: CheckMaterial;

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updateDate: Date;

  @DeleteDateColumn()
  deleteDate: Date;
}
