import { Category } from 'src/categorys/entities/category.entity';
import { OrderItem } from 'src/orders/entities/order-item.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Reports2 {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 64 })
  name: string;

  @Column()
  type: string;

  @Column()
  size: string;

  @Column({ type: 'float' })
  price: number;

  @Column({
    length: '128',
    default: 'No_Image_Available.jpg',
  })
  image: string;

  @CreateDateColumn()
  createAt: Date;

  @UpdateDateColumn()
  updateAt: Date;

  @DeleteDateColumn()
  deleteAt: Date;

  @ManyToOne(() => Category, (category) => category.product)
  category: Category;

  @OneToMany(() => OrderItem, (orderItem) => orderItem.product)
  orderItems: OrderItem[];

  @Column({ default: 1 })
  categoryId: number;
}
