import { IsNotEmpty } from 'class-validator';
export class CreateBillDetailDto {
  @IsNotEmpty()
  bill_detail_name: string;

  @IsNotEmpty()
  bill_detail_amount: number;

  @IsNotEmpty()
  bill_detail_price: number;

  @IsNotEmpty()
  bill_detail_total: number;

  @IsNotEmpty()
  billId: number;

  @IsNotEmpty()
  matId: number;
}
