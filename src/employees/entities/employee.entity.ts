import { Bill } from 'src/bill/entities/bill.entity';
import { CheckTime } from 'src/check-time/entities/check-time.entity';
import { CheckMaterial } from 'src/check_material/entities/check_material.entity';
import { CheckMaterialDetail } from 'src/check_material_detail/entities/check_material_detail.entity';
import { Order } from 'src/orders/entities/order.entity';
import { User } from 'src/users/entities/user.entity';
import {
  Column,
  CreateDateColumn,
  DeleteDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Employee {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  house_no: string;

  @Column()
  village_no: string;

  @Column()
  sub_district: string;

  @Column()
  district: string;

  @Column()
  province: string;

  @Column()
  tel: string;

  @Column()
  email: string;

  @Column()
  position: string;

  @Column()
  hourly_wage: number;

  @Column({
    length: '128',
    default: 'No_Image_Available.jpg',
  })
  image: string;

  // @Column()
  // checkTimeId?: number;

  @OneToOne(() => User, (user) => user.employee)
  @JoinColumn()
  user: User;

  @OneToMany(() => Bill, (bill) => bill.employee)
  bill: Bill[];

  @OneToMany(() => CheckMaterial, (check_material) => check_material.employee)
  check_material: CheckMaterial[];

  @CreateDateColumn()
  createdDate: Date;

  @UpdateDateColumn()
  updateDate: Date;

  @DeleteDateColumn()
  deleteDate: Date;

  @OneToMany(() => CheckTime, (checkTime) => checkTime.employee)
  checkTime: CheckTime;

  // @OneToMany(() => Order, (order) => order.employee)
  // order: Order;
}
